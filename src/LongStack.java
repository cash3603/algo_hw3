import java.util.*;

public class LongStack {

    //changed to private
    private LinkedList<Long> mystack;

    public static void main (String[] argum) {

        System.out.println(interpret("2 15 -"));

    }


    LongStack() {
        mystack = new LinkedList<Long>();

    }

    @Override
//Thrown to indicate that the clone method in class Object has been called to clone an object,
//but that the object's class does not implement the Cloneable interface
    public Object clone() throws CloneNotSupportedException {

        LongStack newstack = new LongStack();
        newstack.mystack = (LinkedList<Long>) mystack.clone();

        return newstack;
    }

    public boolean stEmpty() {
        //Returns the number of elements in this list
        if (mystack.size() == 0) {
            return true;

        }else {
            return false;
        }
    }

    public void push (long a) {

//Pushes an element onto the stack represented by this list.
//In other words, inserts the element at the front of this list
        mystack.push(a);
    }

    public long pop() {
        //Pops an element from the stack represented by this list.
        //In other words, removes and returns the first element of this list

        if (mystack.isEmpty()) {
            //Thrown to indicate that an index of some sort (such as to an array,
            //to a string,
            //or to a vector) is out of range.
            throw  new  IndexOutOfBoundsException("Stack is Empty no elements");
        }
        return this.mystack.pop();
    }

    public void op (String s) {
        //Compares this string to the specified object.
        //The result is true if and only if the argument is not null,
        //and is a String object that represents the same sequence of characters as this object
        if (mystack.size() < 2 ) {
            throw new RuntimeException("Too few elements to perform " + s);
        }

        if (s.equals("+")) {
            this.mystack.push(this.mystack.pop() + this.mystack.pop());

        } else if (s.equals("-")) {
            long element1 = this.pop();
            long element2 = this.pop();
            this.mystack.push(element2 - element1);
        } else if (s.equals("*")) {
            this.mystack.push(this.mystack.pop() * this.mystack.pop());
        } else if (s.equals("/")) {
            long element1 = this.pop();
            long element2 = this.pop();
            this.mystack.push(element2 / element1);
        } else if(s.equals(" ")) {
            return;
        } else {
            throw new RuntimeException("Illegal Operation " + s);
        }
    }


    public long tos() {
        if (stEmpty()) {
            throw new IndexOutOfBoundsException("There is no elements to do the operation");

        }
        return this.mystack.getFirst();
    }


    @Override
    public boolean equals (Object o) {
        return this.mystack.equals(((LongStack)o).mystack);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        Iterator<Long> i = this.mystack.descendingIterator();
        while (i.hasNext()) {
            s.append(i.next());
            s.append(" ");
        }
        return s.toString();
    }

    public static long interpret (String pol) {
        {
            if (pol == null || pol.length() == 0)
                throw new RuntimeException("Expression can't be empty");
            LongStack list = new LongStack();
            StringTokenizer cut = new StringTokenizer(pol, " \t");
            int i = 1;
            int counter = cut.countTokens();
            while (cut.hasMoreTokens())
            {
                String part = (String) cut.nextElement();
                if (part.equals("-") || part.equals("+") || part.equals("/") || part.equals("*")){

                    if (list.mystack.size() < 2) {
                        throw new RuntimeException("Too few numbers in " + pol + "Too perform " + part);

                    }
                    list.op(part);
                }
                else
                {
                    if (counter == i && i > 2)
                        throw new RuntimeException("There is wrong operation in " + pol);
                    try
                    {
                        long a = Long.parseLong(part);
                        list.push(a);
                    }
                    catch(NumberFormatException e)
                    {
                        System.out.println("Expression:  " + pol + "    where " + part + " is not valid");
                    }


                }
                i++;
            }
            if (list.mystack.size() != 1) {
                throw new RuntimeException("Too many numbers in expression " +pol);

            }
            return list.tos();
        }
    }   }